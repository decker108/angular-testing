'use strict';

/* jasmine specs for controllers go here */
describe('myApp controllers', function() {

	beforeEach(module('myApp.controllers'));

	describe('MyCtrl', function(){
		var scope, ctrl;

		beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
	      scope = $rootScope.$new();
	      ctrl = $controller('MyCtrl', {$scope: scope});
	    }));

		it('should set $scope.msg to "Test!!!"', function() {
      		expect(scope.msg).toBe('Test!!!');
    	});
	});
});