'use strict';

/* jasmine specs for controllers go here */
describe('myApp services', function() {

	beforeEach(module('myApp.services'));

	describe('with real deps', function(){
		var service;

		beforeEach(inject(function(MyService) {
      service = MyService;
    }));

		it('should return "Hello World"', function() {
  		expect(service.doWork()).toBe('Hello world');
  	});
	});

	describe('with mock deps', function(){
    var mockMyOtherService = {
      doWork: function() {
        return "test";
      }
    };

    beforeEach(function() {
      module(function($provide) {
        $provide.value('MyOtherService', mockMyOtherService);
      });
    });

		it('should return "test"', inject(function(MyService) {
      expect(MyService.doWork()).toBe('test');
    }));
	});
});
