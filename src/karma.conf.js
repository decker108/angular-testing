module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'lib/angular.js',
      'lib/test/angular-mocks.js',
      'app.js',
      'controller.js',
      'services.js',

      'controller-karma-spec.js',
      'services-karma-spec.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-phantomjs-launcher',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }
  });
};
