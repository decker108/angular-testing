describe('my app', function() {
'use strict';

  describe('view1', function() {

    beforeEach(function() {
      browser.get('http://localhost:8000');
    });

    it('should render the outer message when entering the view', function() {
      var outerMsg = element.all(by.id('outer-msg')).first().getText();
      expect(outerMsg).toMatch("Test!!!");
    });

  });

});