angular.module('myApp.services', [])
.factory('MyService', ['MyOtherService', function(MyOtherService) {
  return {
    doWork: function() {
      return MyOtherService.doWork();
    }
  }
}])
.factory('MyOtherService', [function(MyOtherService) {
  return {
    doWork: function() {
      return "Hello world";
    }
  }
}]);
