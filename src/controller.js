angular.module('myApp.controllers', [])

.controller('MyCtrl', function ($scope) {
	"use strict";

	$scope.msg = "Test!!!";
})
.controller('MyChildCtrl', function($scope) {
	$scope.anotherMsg = "Parentscope msg: " + $scope.msg;
})
;
