# angular-testing

A minimal project to demonstrate setup and organization of Angular.js unit and scenario tests.
Contains end-to-end tests using contractor and unit tests using karma/jasmine.

See https://angular.github.io/protractor/#/ for webdriver and protractor setup instructions.
